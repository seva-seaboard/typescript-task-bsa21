import { elements } from './elements';

const renderRandomMovie = (
    poster: string,
    title: string,
    overview: string
): void => {
    const { randomMovie, randomMovieName, randomMovieDesc, path } = elements;
    randomMovie.style.background = `url(${path}${poster}) no-repeat center`;
    randomMovieName.innerText = `${title}`;
    randomMovieDesc.innerText = `${overview}`;
};

const movies = (data: string | any): void => {
    const { filmContainer, path } = elements;
    const random = Math.floor(Math.random() * data.results.length);
    const { poster_path, original_title, overview } = data.results[random];
    renderRandomMovie(poster_path, original_title, overview);
    filmContainer.innerHTML = '';
    data.results.forEach((item: any) => {
        const poster = item.poster_path
            ? `${path}${item.poster_path}`
            : 'https://images.unsplash.com/photo-1585314062604-1a357de8b000?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80';
        const templ = `
            <div class = "movie-container">
               <div class  = "image-container">
               <img src = ${poster} alt = "movie poster">
               </div>
               <div class = "description">
                    <p class = "title">${item.original_title}</p>
                    <p class = "date">${item.release_date}</p>
               </div>
            </div>
        `;
        filmContainer.insertAdjacentHTML('afterbegin', templ);
    });
};

async function api<T>(url: string): Promise<T> {
    const response = await fetch(url);
    const data = await response.json();
    movies(data);
    return data;
}

export async function render(): Promise<void> {
    // TODO render your app here
    const { form, BASIC_URL, searchInput, POPULAR, btns, UPCOMING, TOP_RATED } =
        elements;
    form.addEventListener('submit', (e) => {
        e.preventDefault();
        api(`${BASIC_URL}&query=${searchInput.value}`);
        searchInput.value = '';
    });
    window.addEventListener('DOMContentLoaded', () => {
        api(`${POPULAR}`);
    });
    btns.forEach((btn) => {
        btn.addEventListener('click', (e) => {
            const target = e.target as HTMLElement;
            if (target.matches('.popular')) {
                api(`${POPULAR}`);
            }
            if (target.matches('.upcoming')) {
                api(`${UPCOMING}`);
            }
            if (target.matches('.top-rated')) {
                api(`${TOP_RATED}`);
            }
        });
    });
}
