export const elements = (() => {
    const KEY = 'api_key=48879fdd7ad2bf956092b9b578fc46e8';
    const BASIC_URL = `https://api.themoviedb.org/3/search/movie?${KEY}`;
    const POPULAR = `https://api.themoviedb.org/3/movie/popular?${KEY}`;
    const UPCOMING = `https://api.themoviedb.org/3/movie/upcoming?${KEY}`;
    const TOP_RATED = `https://api.themoviedb.org/3/movie/top_rated?${KEY}`;

    const path = 'https://image.tmdb.org/t/p/original/';
    const form = document.querySelector('.form-inline') as HTMLFormElement;
    const searchInput = document.querySelector('#search') as HTMLInputElement;
    const btns = document.querySelectorAll('.btn');
    const filmContainer = document.querySelector(
        '.film-container'
    ) as HTMLElement;
    const randomMovie = document.querySelector('#random-movie') as HTMLElement;
    const randomMovieName = document.querySelector(
        '#random-movie-name'
    ) as HTMLElement;
    const randomMovieDesc = document.querySelector(
        '#random-movie-description'
    ) as HTMLElement;

    return {
        BASIC_URL,
        POPULAR,
        UPCOMING,
        TOP_RATED,
        path,
        form,
        searchInput,
        filmContainer,
        btns,
        randomMovie,
        randomMovieName,
        randomMovieDesc,
    };
})();
